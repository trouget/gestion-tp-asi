<?php 
  include '../public/header.php';
?>

<div class="card card-nav-tabs text-center">
  <div class="card-header card-header-primary">
    Création d'un apprenti
</div>

<?php
if(isset($message))
{
echo '<script type="text/javascript">
  <!--
    alert("'.$message.'");
  //-->
  </script> </p>';
}
?>
  <form action="../classes/utilisateurs.php" method="post">
      <div class="form-group">
        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" required="required">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required="required">
      </div>
    <div class="form-group">
      <input type="text" class="form-control" id="id" name="id" placeholder="Identifiant" required="required">
    </div>
    <div class="form-group">
      <input type="email" class="form-control" id="Email" name="Email" placeholder="Entrer Email" required="required">
    </div>
    <div class="form-group">
      <input type="password" class="form-control" id="motdepasse" name="motdepasse" placeholder="Mot de passe" required="required">
    </div>
    <div class="form-group">
      <input type="password" class="form-control" id="confmotdepasse" name= "confmotdepasse" placeholder="confirmer Mot de passe" required="required">
    </div>
    <button type="submit" class="btn btn-primary" value="creer">Créer</button>
  </form>
</div>

<?php
include '../public/header.php';
?>

</head>

<SCRIPT LANGUAGE="JavaScript">
            function message() {
                alert($message);
            }
        </SCRIPT>
