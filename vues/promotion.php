<?php
include '../public/header.php';
?>
<body>
<a href="creation-promotion.php"><button class="btn btn-primary btn-round">Créer une Promotion</button></a>    <br><br>
    <div class="card card-nav-tabs text-center">
        <div class="card-header card-header-primary">
            Gestion Promotion
        </div>
        <?php
            require ('../database/config.php');

              //RÉCUPÉRATION DES PROMOTIONS (TABLEAU)
              $sql1 = 'SELECT * FROM Promotion';
              $response=$connection->query($sql1);
              $lesPromotions=$response->fetchAll();
          ?>

<table class="table table-sm">
            <thead>
              <tr>
                  <th scope="col">Libelle</th>
                  <th scope="col">Modification / Suppression</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?php foreach($lesPromotions as $promotion) { ?>
                <td><?php echo $promotion['libelle_promo']; ?> </td>
                <td>
                  <button type="submit" rel="tooltip" class="btn btn-success btn-round">
                      <i class="material-icons">edit</i>
                  </button>
                    <form action="../classes/delete_promotion.php" method="POST">
                      <input type="hidden" name="id_promotion" value="<?php echo $promotion['id_promotion'];?>">
                  <button type="submit" rel="tooltip" class="btn btn-danger btn-round">
                      <i class="material-icons">cancel</i>
                  </button>
                    </form>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
    </div>
    </div>
  </div>
</body>
</html>
