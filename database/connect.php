<?php
if(!isset($_GET['login']) && !isset($_GET['motdepasse']))
{
    header('Location: index.php');
}
else
{
    // On va vérifier les variables
    if(!preg_match('/^[[:alnum:]]+$/', $_GET['login']) or
!preg_match('/^[[:alnum:]]+$/', $_GET['motdepasse']))
    {
        echo 'Vous devez entrer uniquement des lettres ou des chiffres <br/>';
        echo '<a href="/gestion-tp-asi/public/index.php" temp_href="/gestion-tp-asi/public/index.php">Réessayer</a>';
        exit();
    }
    else
    {

        if ($_GET['login'] == "admin")
        {

            if ($_GET['motdepasse'] != "admin")
            {
                echo 'mauvais mot de passe <br/>';
                echo '<a href="/gestion-tp-asi/public/index.php" temp_href="/gestion-tp-asi/public/index.php">Réessayer</a>';
                exit();
            }
            else
            {
                header('Location: /gestion-tp-asi/vues/formateur.php');
            }
        

        }
        else
        {

        
            require('config.php'); // On réclame le fichier

            $login = $_GET['login'];
            $motdepasse = $_GET['motdepasse'];

            $sql = "SELECT * FROM Apprenti WHERE identifiant='".mysqli_escape_string($connection, $login)."'";

            // On vérifie si ce login existe
            $requete_1 = mysqli_query($connection, $sql) or die ( mysql_error() );

            if(mysqli_num_rows($requete_1)==0)
            {
                echo 'Ce login est incorrect ! <br/>';
                echo '<a href="/gestion-tp-asi/public/index.php" temp_href="/gestion-tp-asi/public/index.php">Réessayer</a>';
                exit();
            }
             else
            {
                 // On vérifie si le login et le mot de passe correspondent au compte utilisateur
                $requete_2 = mysqli_query($connection, $sql." AND mot_de_passe='".mysqli_escape_string($connection, $motdepasse)."'")
                or die ( mysql_error() );

                if (mysqli_num_rows($requete_2)==0)
                {
                    echo 'Le mot de passe est incorrect ! <br/>';
                    echo '<a href="/gestion-tp-asi/public/index.php" temp_href="/gestion-tp-asi/public/index.php">Réessayer</a>';
                    exit();
                }
                else
                {
                
                // On redirige vers la partie membre
                header('Location: /gestion-tp-asi/vues/apprenti.php');
                }

            }
        }
 
    }    
}
?>